#pragma once

class Fraction {
private: 
	int _num;
	int _den;

public:
	Fraction() : _num(1), _den(1) {}
	Fraction(int, int);
	Fraction(Fraction const &number);
    
    // is this Ok or maybe better this:   int getNum() const { return _num; }    ??
	const int* getNum() const { return &_num; }
	const int* getDen() const { return &_den; }

	void setNum(int);
	void setDen(int);

	void add(Fraction b);
	void subtruct(Fraction b);
	void multiply(Fraction b);
	void devide(Fraction b);
 
	static void com_denom(Fraction &a, Fraction &b);
	double decimal() { return (double)_num / (double)_den; }
}; 