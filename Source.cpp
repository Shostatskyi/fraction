#include "Fraction.h"
#include <iostream>

using namespace std;

void main() {
	Fraction a(8, 12), b(11, 18);
	a.add(b);
	cout << *a.getNum() << " / " << *a.getDen() << endl;
	//Fraction c(1, 0);

	system("pause");
}